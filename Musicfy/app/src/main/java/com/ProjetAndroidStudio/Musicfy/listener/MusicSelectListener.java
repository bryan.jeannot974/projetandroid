package com.ProjetAndroidStudio.Musicfy.listener;

import com.ProjetAndroidStudio.Musicfy.model.Music;

import java.util.List;

public interface MusicSelectListener {
    void playQueue(List<Music> musicList);

    void addToQueue(List<Music> music);

}