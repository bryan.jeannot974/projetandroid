package com.ProjetAndroidStudio.Musicfy.listener;

import com.ProjetAndroidStudio.Musicfy.model.Album;

public interface AlbumSelectListener {
    void selectedAlbum(Album album);
}
