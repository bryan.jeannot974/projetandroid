package com.ProjetAndroidStudio.Musicfy.player;

public class PlayerProgressEvent {
    public final int percent;

    public PlayerProgressEvent(int percent) {
        this.percent = percent;
    }
}
